## Usage



[bin/zookeeper-server-start](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/zookeeper-server-start) [etc/kafka/zookeeper.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/kafka/zookeeper.properties)

[bin/kafka-server-start](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/kafka-server-start) [etc/kafka/server.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/kafka/server.properties)

[bin/connect-standalone](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/connect-standalone) [etc/kafka/connect-standalone.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/kafka/connect-standalone.properties) [etc/kafka/thiqah-sqlserver-cdc.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/kafka/thiqah-sqlserver-cdc.properties) [etc/kafka/thiqah-http-sink.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/kafka/thiqah-http-sink.properties)

[bin/kafka-console-consumer](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/kafka-console-consumer) --topic serverName.schemaName.tableName --bootstrap-server localhost:9092

[bin/ksql-server-start](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/ksql-server-start) [etc/ksqldb/ksql-server.properties](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/etc/ksqldb/ksql-server.properties)

[bin/ksql](https://gitlab.com/lamoboos223/sql-server-kafka-connect/-/blob/master/bin/ksql) http://0.0.0.0:8088


### KSQLDB Commands

> VALUE_FORMAT COULD BE `AVRO` `JSON` `DELIMITED`

```
CREATE STREAM PARENT_STREAM (ID INT, NAME VARCHAR, LOCATION VARCHAR) WITH (KAFKA_TOPIC = 'thiqah.dbo.Company', PARTITIONS = 1, VALUE_FORMAT = 'JSON');

SET 'auto.offset.reset' = 'earliest';

SELECT ID, NAME, LOCATION FROM PARENT_STREAM EMIT CHANGES;

SHOW TOPICS;

show streams;

INSERT INTO PARENT_STREAM (ID, NAME, LOCATION) VALUES (1, 'LAMA', 'KSA');


create stream CHILD_STREAM AS SELECT ID, NAME, LOCATION FROM PARENT_STREAM WHERE LOCATION='KSA';

PRINT CHILD_STREAM FROM BEGINNING LIMIT 4;


CREATE STREAM WEEKENDSTREAM ("PAYLOAD" STRUCT < before STRUCT <ID INT, NAME VARCHAR, LOCATION VARCHAR>, after STRUCT <ID INT, NAME VARCHAR, LOCATION VARCHAR> >) WITH (KAFKA_TOPIC='weekend.dbo.Company', VALUE_FORMAT = 'JSON');

select PAYLOAD->BEFORE->ID as BEFORE_ID, PAYLOAD->BEFORE->NAME as BEFORE_NAME, PAYLOAD->BEFORE->LOCATION as BEFORE_LOCATION, PAYLOAD->AFTER->ID as ID, PAYLOAD->AFTER->NAME as NAME, PAYLOAD->AFTER->LOCATION as LOCATION from WEEKENDSTREAM EMIT CHANGES;

```

---

## Database CDC

```sh

docker run --name SQL19 -p 1433:1433  -e "ACCEPT_EULA=Y"  -e "SA_PASSWORD=Qwert#1234"  -e "MSSQL_AGENT_ENABLED=True" -v ~/SqlDockerVol/userdatabase:/userdatabase  -v ~/SqlDockerVol/sqlbackups:/sqlbackups -d mcr.microsoft.com/mssql/server:2019-latest

```

https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash


---

```sql

EXEC sys.sp_cdc_enable_db

EXEC sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name   = N'MyTable', 
@role_name     = N'MyRole',  
@filegroup_name = N'MyDB_CT',
@supports_net_changes = 0

```

---

## SQL Servre Properties

- [sql server cdc file](/etc/kafka/thiqah-sqlserver-cdc.properties)
- [sql server configuration from Debezium](https://debezium.io/documentation/reference/connectors/sqlserver.html#:~:text=The%20following%20configuration%20properties%20are%20required%20unless%20a%20default%20value%20is%20available.)
