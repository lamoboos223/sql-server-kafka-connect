-- CREATE TABLE Company (ID INT identity(1, 1), NAME VARCHAR(50), LOCATION VARCHAR(100))
-- drop table Company;
-- delete Company where 1=1;
-- insert into Company VALUES ('Sara', 'KSA')
-- select * from Company;

-- update Company set NAME='THIQAH' where ID=25;
-- EXEC sys.sp_cdc_enable_db

-- EXEC sys.sp_cdc_enable_table
-- @source_schema = N'dbo',
-- @source_name   = N'Company', 
-- @role_name     = N'admin',  
-- @supports_net_changes = 0